<!DOCTYPE html>

<html lang="en">

	<head>

		<meta charset="utf-8">

	 	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    	<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="title" content="Bozhidar Atanasovski" />

		<meta name="description" content="My personal portfolio" />

		<meta name="keywords" content="project, awards, vue, gsap, portfolio, cv, laravel, php, javascript, animations, css, sass" />

		<meta name="theme-color" content="#131417"/>

		<link rel="apple-touch-icon" sizes="57x57" href="{{ url('/images/apple-icon-57x57.pngs') }}">

		<link rel="apple-touch-icon" sizes="60x60" href="{{ url('/images/apple-icon-60x60.png') }}">

		<link rel="apple-touch-icon" sizes="72x72" href="{{ url('/images/apple-icon-72x72.png') }}">

		<link rel="apple-touch-icon" sizes="76x76" href="{{ url('/images/apple-icon-76x76.png') }}">

		<link rel="apple-touch-icon" sizes="114x114" href="{{ url('/images/apple-icon-114x114.png') }}">

		<link rel="apple-touch-icon" sizes="120x120" href="{{ url('/images/apple-icon-120x120.png') }}">

		<link rel="apple-touch-icon" sizes="144x144" href="{{ url('/images/apple-icon-144x144.png') }}">

		<link rel="apple-touch-icon" sizes="152x152" href="{{ url('/images/apple-icon-152x152.png') }}">

		<link rel="apple-touch-icon" sizes="180x180" href="{{ url('/images/apple-icon-180x180.png') }}">

		<link rel="icon" type="image/png" sizes="192x192"  href="{{ url('/images/android-icon-192x192.png') }}">

		<link rel="icon" type="image/png" sizes="32x32" href="{{ url('/images/favicon-32x32.png') }}">

		<link rel="icon" type="image/png" sizes="96x96" href="{{ url('/images/favicon-96x96.png') }}">

		<link rel="icon" type="image/png" sizes="16x16" href="{{ url('/images/favicon-16x16.png') }}">

		<link rel="manifest" href="/manifest.json">

		<meta name="msapplication-TileColor" content="#ffffff">

		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">

		<meta name="theme-color" content="#ffffff">

		<link rel="manifest" href="{{ url('/manifest.json') }}">

		<link href="https://fonts.googleapis.com/css?family=Chivo:300,700|Playfair+Display:700i" rel="stylesheet">
		
    	<link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">

    	<title>Bozhidar Atanasovski</title>
  		
		<script>
	        if('serviceWorker' in navigator) {
	          	window.addEventListener('load', () => {
	              	navigator.serviceWorker.register('/sw.js')
	          	})
	        }
	    </script>
	</head>

	<body>
		
		<div id="app">

			<index id="index"></index>
		
		</div>

		<script type="text/javascript" src="{{ url('js/app.js') }}"></script>

	</body>

</html>
