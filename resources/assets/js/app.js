// Create vue instance
window.Vue = require("vue");
// Create image particles
window.ImageParticles = require("../imageParticles/App.js");
// Init image particles
window.imageParticles = new ImageParticles.default();

// Comment following lines in development to see error logs
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.performance = true;
Vue.config.silent = true;

import Index from "../../components/Index";

const index = new Vue({
    el: "#index",
    components: { Index },
});
