import EventEmitter from "events";
import * as THREE from "three";
import browser from "browser-detect";
import { passiveEvent } from "../../utils/event.utils.js";

export default class InteractiveControls extends EventEmitter {
    get enabled() {
        return this._enabled;
    }

    constructor(camera, el) {
        super();

        this.camera = camera;

        this.el = el;

        this.plane = new THREE.Plane();

        this.raycaster = new THREE.Raycaster();

        this.mouse = new THREE.Vector2();

        this.offset = new THREE.Vector3();

        this.intersection = new THREE.Vector3();

        this.objects = [];

        this.hovered = null;

        this.selected = null;

        this.isDown = false;

        this.browser = browser();

        this.enable();
    }

    enable() {
        if (this.enabled) return;

        this.addListeners();

        this._enabled = true;
    }

    disable() {
        if (!this.enabled) return;

        this.removeListeners();

        this._enabled = false;
    }

    addListeners() {
        this.handlerMove = this.onMove.bind(this);

        this.handlerLeave = this.onLeave.bind(this);

        if (this.browser.mobile) {
            this.el.addEventListener("touchmove", this.handlerMove, passiveEvent);
        } else {
            this.el.addEventListener("mousemove", this.handlerMove);

            this.el.addEventListener("mouseleave", this.handlerLeave);
        }
    }

    removeListeners() {
        if (this.browser.mobile) {
            this.el.removeEventListener("touchmove", this.handlerMove);
        } else {
            this.el.removeEventListener("mousemove", this.handlerMove);

            this.el.removeEventListener("mouseleave", this.handlerLeave);
        }
    }

    resize(x, y, width, height) {
        x || y || width || height ? (this.rect = { x, y, width, height }) : (this.rect = this.el.getBoundingClientRect());
    }

    onMove(e) {
        const t = e.touches ? e.touches[0] : e;

        const touch = { x: t.clientX, y: t.clientY };

        this.mouse.x = ((touch.x + this.rect.x) / this.rect.width) * 2 - 1;

        this.mouse.y = -((touch.y + this.rect.y) / this.rect.height) * 2 + 1;

        this.raycaster.setFromCamera(this.mouse, this.camera);

        const intersects = this.raycaster.intersectObjects(this.objects);

        if (intersects.length > 0) {
            const object = intersects[0].object;

            this.intersectionData = intersects[0];

            this.plane.setFromNormalAndCoplanarPoint(this.camera.getWorldDirection(this.plane.normal), object.position);

            if (this.hovered !== object) {
                this.emit("interactive-out", { object: this.hovered });

                this.emit("interactive-over", { object });

                this.hovered = object;
            } else {
                this.emit("interactive-move", { object, intersectionData: this.intersectionData });
            }
        } else {
            this.intersectionData = null;

            if (this.hovered !== null) {
                this.emit("interactive-out", { object: this.hovered });

                this.hovered = null;
            }
        }
    }

    onLeave(e) {
        this.emit("interactive-out", { object: this.hovered });

        this.hovered = null;
    }
}
