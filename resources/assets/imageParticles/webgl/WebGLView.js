import * as THREE from "three";
import InteractiveControls from "./controls/InteractiveControls";
import Particles from "./particles/Particles";

export default class WebGLView {
    constructor(app, wrapper) {
        this.app = app;

        this.wrapper = wrapper;

        this.initThree();

        this.initParticles();

        this.initControls();

        this.particles.init(require("../../../../public/images/sample-6.png"));
    }

    initThree() {
        // Create scene
        this.scene = new THREE.Scene();
        // Create camera
        this.camera = new THREE.PerspectiveCamera(50, this.wrapper.offsetWidth / this.wrapper.offsetHeight, 1, 10000);
        // Set camera position
        this.camera.position.z = 500;
        // Create renderer
        this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        // Create clock
        this.clock = new THREE.Clock(true);
    }

    initControls() {
        this.interactive = new InteractiveControls(this.camera, this.renderer.domElement);
    }

    initParticles() {
        this.particles = new Particles(this);

        this.scene.add(this.particles.container);
    }

    update() {
        const delta = this.clock.getDelta();

        if (this.particles) this.particles.update(delta);
    }

    draw() {
        this.renderer.render(this.scene, this.camera);
    }

    hide() {
        this.particles.hide(false, 0.5);
    }

    show() {
        this.particles.show(0.5);
    }

    resize(element) {
        if (!this.renderer) return;

        if (element) {
            this.camera.aspect = element.offsetWidth / element.offsetHeight;

            this.camera.updateProjectionMatrix();

            this.fovHeight = 2 * Math.tan((this.camera.fov * Math.PI) / 180 / 2) * this.camera.position.z;

            this.renderer.setSize(element.offsetWidth, element.offsetHeight);

            if (this.interactive) this.interactive.resize();

            if (this.particles) this.particles.resize();
        }
    }
}
