import WebGLView from "./webgl/WebGLView";

export default class App {
    constructor() {}

    init(wrapper) {
        // Create html wrapper
        this.wrapper = wrapper;
        // Create new webgl view for the wrapper
        this.webgl = new WebGLView(this, this.wrapper);
        // Add webgl element to the wrapper
        this.wrapper.appendChild(this.webgl.renderer.domElement);
        // Create event listeners
        this.handlerAnimate = this.animate.bind(this);
        // Add event listeners
        window.addEventListener("resize", this.resize.bind(this));
        // Set animation frame
        this.animate();
        // Set window frame
        this.resize();
    }

    animate() {
        // Update clock
        this.webgl.update();
        // Render the canvas
        this.webgl.draw();
        // Add animation
        this.raf = requestAnimationFrame(this.handlerAnimate);
    }

    resize() {
        // On window resize
        this.webgl.resize(this.wrapper);
    }

    show() {
        // Create particles
        this.webgl.show();
    }

    hide() {
        // Destroy particles
        this.webgl.hide();
    }
}
