const mix = require("laravel-mix");

mix.js("resources/assets/js/app.js", "public/js");

mix.sass("resources/assets/sass/app.css", "public/css");

require("./glslify");

mix.shader();
