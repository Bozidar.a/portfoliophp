module.exports = {
    // Print semicolons at the ends of statements.
    semi: true,
    // trailing comma on last array element or object
    trailingComma: "all",
    // use double quotes for strings
    singleQuote: false,
    // set page width to 200 chars, because we all use wider screens
    printWidth: 200,
    // tab should use 4 spaces, same as in editorconfig
    tabWidth: 4,
    // Print spaces between brackets in object literals.
    bracketSpacing: true,
    // Whether or not to indent the code inside <script> and <style> tags in Vue files.
    vueIndentScriptAndStyle: true,
};
