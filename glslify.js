let mix = require('laravel-mix')

class ShaderPlugin {

    webpackConfig(config) {

       config.module.rules.push({

            test: /\.(glsl|frag|vert)$/,

            loader: ['glslify-import-loader', 'raw-loader', 'glslify-loader'],

            exclude: /(node_modules)/
        },{

            test: /\.(js|jsx)$/,

            loader: 'babel-loader',

            exclude: /(node_modules)/
        })
    }
}

mix.extend('shader', new ShaderPlugin())