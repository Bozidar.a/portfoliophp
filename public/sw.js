let filesToCache = [
    
    'css/app.css',
    'js/app.js',

    'images/map.png',
    'images/projects/editor-1.png',
    'images/projects/editor-2.png',
    'images/projects/editor-3.png',
    'images/projects/experten.png',
    'images/projects/getPR.png',
    'images/projects/infomail.png',
    'images/projects/Koli.png',
    'images/projects/letimoCH.png',
    'images/projects/newsmarket.png',
    'images/projects/tabellarius.png',
    'images/projects/tabellariusMK.png',
    'images/projects/validate.png',

    'images/design.svg',
    'images/develop.svg',
    'images/hosting.svg',
    'images/icon-computer.svg',
    'images/seo.svg'
]

let cacheName = 'cache30072021'

self.skipWaiting()

self.addEventListener('install', event => {

  	event.waitUntil(Promise.all([caches.open(cacheName), self.skipWaiting()]).then(cache => {

      	var static_cache = cache[0]

        return Promise.all([static_cache.addAll(filesToCache)])
    }))
})

self.addEventListener('fetch', event => {

    if(!(event.request.url.indexOf('http') === 0)) return

    if(event.request.cache === 'only-if-cached' && event.request.mode !== 'same-origin') return

    event.respondWith(caches.match(event.request).then(response => {

        if(response) { return response }

        return fetch(event.request).then(response => {

            return caches.open(cacheName).then(cache => {

                if(!event.request.url.includes('admin')) {

                    if(!event.request.referrer.includes('admin')) {

                        cache.put(event.request.url, response.clone())
                    }                
                }

                return response
            })
        })
    }))
})

self.addEventListener('activate', event => {

     event.waitUntil(Promise.all([self.clients.claim(), 

        caches.keys().then(names => {

            return Promise.all(names.map(name => {

                if(name != cacheName) {

                    return caches.delete(name)
                }
            }))
        })
    ]))
})
