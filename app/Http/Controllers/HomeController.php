<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index() 
    {
        return view('index');
    }

    public function downloadPDF() {

        $file = env('PUBLIC_PATH', base_path('public')) . ($path ? '/' . $path : $path) . "/resume/CV_Bozhidar_Atanasovski.pdf";

        $headers = ['Content-Type' => 'application/pdf'];

        return response()->download($file, 'test.pdf', $headers);
    }
}
